<?php 
$url = "https://opensky-network.org/api/states/all";
$json = file_get_contents($url);
$json_decoded = json_decode($json);

?>  
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="vendor/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
  #map_wrapper {
    height: 600px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}
</style>
</head>


<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="stats.html">Flight Stats</a>
            </div>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href="stats.php"><i class="fa fa-plane fa-fw"></i>Flight Stats</a>
                        </li>
                        <li>
                            <a href="search.php"><i class="fa fa-search fa-fw"></i>Search Flight</a>
                        </li>
                        <li>
                            <a href="sky.php"><i class="fa fa-search fa-fw"></i>Air Map</a>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Flights</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Search Flights
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                        
                        <div id="map_wrapper">
    <div id="map_canvas" class="mapping"></div>
</div>

                        <script type="text/javascript">
                          
                          jQuery(function($) {
                            // Asynchronously Load the map API 
                            var script = document.createElement('script');
                            script.src = "//maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
                            document.body.appendChild(script);
                        });

                        function initialize() {
                            var map;
                            var bounds = new google.maps.LatLngBounds();
                            var mapOptions = {
                                mapTypeId: 'roadmap'
                            };
                                            
                            // Display a map on the page
                            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
                            map.setTilt(45);
                                
                            // Multiple Markers
                            var markers = [
                             <?php foreach ($json_decoded->states as $key => $value) { ?>
                                ['<?php echo $value[1]; ?>',<?php echo $value[5]; ?>,<?php echo $value[6]; ?>],
                                <?php }  ?>
                            ];
                                                
                            // Info Window Content
                            var infoWindowContent = [
                                 <?php foreach ($json_decoded->states as $key => $value) { ?>
                                ['<div class="info_content">' + '<h3><?php echo $value[2]; ?></h3>' + '<p>flight Number - <?php echo $value[1]; ?></p>' + '<p>altitude - <?php echo $value[7]; ?></p>' + '<a href="process.php?code=<?php echo $value[1]; ?>">view</a>' + '</div>'],
                                <?php }  ?>
                                
                            ];

                             var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
                                var icon = {
                                url: "plane3.png", // url
                                scaledSize: new google.maps.Size(30, 30), // scaled size
                                origin: new google.maps.Point(0,0), // origin
                                anchor: new google.maps.Point(0, 0) // anchor
                            };
                            // Display multiple markers on a map
                            var infoWindow = new google.maps.InfoWindow(), marker, i;
                            
                            // Loop through our array of markers & place each one on the map  
                            for( i = 0; i < markers.length; i++ ) {
                                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                                bounds.extend(position);
                                marker = new google.maps.Marker({
                                    position: position,
                                    map: map,
                                    icon: icon,
                                    title: markers[i][0]
                                });
                                
                                // Allow each marker to have an info window    
                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                    return function() {
                                        infoWindow.setContent(infoWindowContent[i][0]);
                                        infoWindow.open(map, marker);
                                    }
                                })(marker, i));

                                // Automatically center the map fitting all markers on the screen
                                map.fitBounds(bounds);
                            }

                            // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
                            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
                                this.setZoom(8);
                                google.maps.event.removeListener(boundsListener);
                            });
                            
                        }
                        </script>


                            
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
            
           
    </div>
    <!-- /#wrapper -->


    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

   
    <!-- Custom Theme JavaScript -->
    <script src="vendor/js/sb-admin-2.js"></script>

</body>

</html>
