function smallAirportMapSetup(mapId, airportLat, airportLng){
	map = new L.Map(mapId, {maxZoom: 11});
	
	var tiles = new L.TileLayer(
			  'http://maptiles-{s}.flightstats.com/surfer/{z}/{x}/{y}.png',
			  { minZoom: 0, maxZoom: 15,
				attribution: '<a target="_blank" href="http://maptiles-a.flightstats.com/attribution.html">Attribution</a>',
			    subdomains: 'abcd' });

	map.attributionControl.setPrefix('');
	map.setView(new L.LatLng(airportLat, airportLng), 12);
	map.addLayer(tiles);
	
	var airportIcon = createAirportIcon();
	var airportMarker = new L.Marker(new L.LatLng(airportLat, airportLng), { clickable:false, icon: airportIcon });
	map.addLayer(airportMarker);
}

function createAirportIcon(){
    var airportIcon = L.icon({
        iconUrl: '//dem5xqcn61lj8.cloudfront.net/FlightTracker/icon_airport_depart.png',
        iconSize: [36, 36],
        iconAnchor: [18, 18]
    });
	
	return airportIcon;
}