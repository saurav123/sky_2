<?php
error_reporting(E_ALL);

include_once "simple_html_dom.php";

if (!empty($_POST)) {
    //print_r($_POST); die;
    $flight_company = urlencode($_POST['company']);
    $flight_number  = urlencode($_POST['number']);
    $data           = 'http://www.flightstats.com/go/FlightStatus/flightStatusByFlight.do?airline=' . $flight_company . '&flightNumber=' . $flight_number . '&submit=Search+Flight';

}
$html = file_get_html($data);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="vendor/SmallAirportMap.js"></script>
    <script src="vendor/leaflet.js"></script>

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="vendor/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <style type="text/css">
        .uiComponent332x250 {
    width: 332px;
    height: 250px;
    margin: 0;
    padding: 0 0 10px;
    text-align: left;
}
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="stats.html">Flight Stats</a>
            </div>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="stats.php"><i class="fa fa-plane fa-fw"></i>Flight Stats</a>
                        </li>
                        <li>
                            <a href="search.php"><i class="fa fa-search fa-fw"></i>Search Flight</a>
                        </li>
                        <li>
                            <a href="sky.php"><i class="fa fa-search fa-fw"></i>Air Map</a>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Flights</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Search Flights
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                                <?php
foreach ($html->find('.logo') as $e) {
    echo "logo = " . $e . '<br>';
}

foreach ($html->find('.route') as $e) {
    echo "route = " . $e . '<br>';
}

foreach ($html->find('.flightName') as $e) {
    echo "flightName = " . $e . '<br>';
}

foreach ($html->find('.statusDetailsTable') as $e) {
    echo "statusDetailsTable = " . $e . '<br>';
}
foreach ($html->find('.contentTable') as $e) {
    echo "contentTable = " . $e . '<br>';
}
?>
<div> <?php 
foreach ($html->find('.uiComponent332x250') as $e) {
    echo $e . '<br>';
}
?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="vendor/js/sb-admin-2.js"></script>

</body>
</html>
